#!/usr/bin/env python3
# vim:fileencoding=utf-8
#
# This file is part of FreeDB2MusicBrainz.py.
#
# Copyright © 2017 Frederik “Freso” S. Olesen <https://freso.dk/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""Tests for ``cddb_lookup_string()``."""

import unittest
from freedb2musicbrainz import cddb_lookup_string


class TestCddbLookupString(unittest.TestCase):
    """Tests the functionality of ``cddb_lookup_string()``."""

    def setUp(self):
        class MockDisc():
            def __init__(self, freedb_id, last_track_num, tracks, seconds):
                self.freedb_id = freedb_id
                self.last_track_num = last_track_num
                self.tracks = tracks
                self.seconds = seconds

        self.empty_disc = MockDisc(
            freedb_id='',
            last_track_num=0,
            tracks={},
            seconds=0,
        )

    def test_call_with_no_arguments(self):
        self.assertRaisesRegex(TypeError, 'missing 1 required positional argument',
                               cddb_lookup_string)

    def test_lookup_string_generation(self):
        self.assertEqual(cddb_lookup_string(self.empty_disc), ' 0  0')


if __name__ == '__main__':
    unittest.main()
